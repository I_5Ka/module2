using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");
        }

        public int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            return (companiesNumber * companyRevenue * tax / 100);
        }

        public string GetCongratulation(int input)
        {
            if (input >= 18 && input % 2 == 0)
            {
                return ("���������� � ����������������!");
            }
            else if (input > 12 && input % 2 == 1 && input < 18)
            {
                return ("���������� � ��������� � ������� �����!");
            }
            else return ("���������� � " + input + "-������!");
        }

        public double GetMultipliedNumbers(string first, string second)
        {
            double fd, sd;
            first = first.Replace('.', ',');
            second = second.Replace('.', ',');
            bool success1 = double.TryParse(first, out fd);
            bool success2 = double.TryParse(second, out sd);
            if (success1 && success2)
            {
                return (fd * sd);
            }
            throw new NotImplementedException();
        }

        public static double GetFigureValues(Figure figureType, Parameter parameterToCompute, Dimensions dimensions)
        {
            switch (parameterToCompute)
            {
                case Parameter.Perimeter:
                    switch (figureType)
                    {
                        case Figure.Circle:
                            if (dimensions.Radius != 0)
                            {
                                return Math.Round((2 * Math.PI * dimensions.Radius));
                            }
                            else if (dimensions.Diameter != 0)
                            {
                                return Math.Round(Math.PI * dimensions.Diameter);
                            }
                            else throw new NotImplementedException();
                        case Figure.Rectangle:
                            return Math.Round((dimensions.FirstSide + dimensions.SecondSide) * 2);
                        case Figure.Triangle:
                            return Math.Round(dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide);
                        default: throw new NotImplementedException();
                    }
                case Parameter.Square:
                    switch (figureType)
                    {
                        case Figure.Circle:
                            if (dimensions.Radius != 0)
                            {
                                return Math.Round(Math.PI * Math.Pow(dimensions.Radius, 2));
                            }
                            else if (dimensions.Diameter != 0)
                            {
                                return Math.Round(Math.PI * Math.Pow(dimensions.Diameter / 2, 2));
                            }
                            else throw new NotImplementedException();
                        case Figure.Rectangle:
                            return Math.Round(dimensions.FirstSide * dimensions.SecondSide);
                        case Figure.Triangle:
                            if (dimensions.FirstSide != 0 && dimensions.Height != 0)
                            {
                                return Math.Round(dimensions.FirstSide * dimensions.Height / 2);
                            }
                            else if (dimensions.FirstSide != 0 && dimensions.SecondSide != 0 && dimensions.ThirdSide != 0)
                            {
                                double p = (dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide) / 2;
                                return Math.Round(Math.Sqrt(p * (p - dimensions.FirstSide) * (p - dimensions.SecondSide) * (p - dimensions.ThirdSide)));
                            }
                            else throw new NotImplementedException();
                        default: throw new NotImplementedException();
                    }
                default: throw new NotImplementedException();
            }
        }
    }
}
